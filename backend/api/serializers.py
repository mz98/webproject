from rest_framework import serializers
from .models import User,Class, Quiz, Question, Choice, GradedQuiz
from rest_framework.authtoken.models import Token

class StringSerializer(serializers.StringRelatedField):
    def to_internal_value(self, value):
        return value

class UserSerializer(serializers.ModelSerializer):   
    # id = serializers.SerializerMethodField()
 
    class Meta:
        model = User
        fields = ('id', 'username','email', 'password', 'is_student', 'is_teacher')
        extra_kwargs = {'password':{'write_only':True, 'required':True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user        

    # return render(request, 'template.html', context=context) 
    # def get_id(self,obj):
    #     print(obj)
        # print("getquizques")
        # questions = QuestionSerializer(obj.questions.all(), many=True).data
        # print(questions)
        # return questions
    # def update(self,request, instance):
    #     # print(instance)
    #     # print(validated_data)
    #     # return instance
    #     print(request)
    #     data = request
    #     print(data)
    #     print(data.username)
    #     # token = request.get('token', instance.token)
    #     # print(token)
    #     print(instance)
    #     # instance.code = validated_data.get('code', instance.code)
    #     # instance.linenos = validated_data.get('linenos', instance.linenos)
    #     # instance.language = validated_data.get('language', instance.language)
    #     # instance.style = validated_data.get('style', instance.style)
    #     # instance.save()
    #     return instance

class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ('__all__')

class QuestionSerializer(serializers.ModelSerializer):
    choices = ChoiceSerializer(many=True)
    answer = ChoiceSerializer(many=False)

    class Meta:
        model = Question
        fields = ('id', 'choices','answer', 'question', 'order')

class ClassSerializer(serializers.ModelSerializer):
    teacher = UserSerializer(many=False)
    # quiz = QuizSerializer(many=True)
    students = UserSerializer(many=True, required=False)

    class Meta:
        model = Class
        fields = ['id','title','description','teacher','students']

    def create(self, request):
        data = request.data
        clas = Class()
        # user = Token.objects.filter(key=data['token'])
        # if user.exists():
        #     user = user.last().user
        # teacher = user
        teacher = User.objects.filter(username=data['teacher'])
        if len(teacher) != 0:
            clas.teacher = teacher[0]
            clas.title = data['title']
            clas.description = data['description']
            clas.save()

            for q in data['students']:
                stud = User.objects.filter(username=q)
                if(len(stud) != 0):
                    for i in stud:
                        clas.students.add(i)
                else:
                    Class.objects.filter(id=clas.id).delete()
                    return False
            clas.save()
            return clas
        else:
            Class.objects.filter(id=clas.id).delete()
            return False

class QuizSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField()
    # questions = QuestionSerializer(many=True)
    teacher = UserSerializer(many=False)
    clas = ClassSerializer(many=False)

    class Meta:
        model = Quiz
        fields = ('__all__')

    def get_questions(self, obj):
        # print("getquizques")
        questions = QuestionSerializer(obj.questions.all(), many=True).data
        # print(questions)
        return questions

    def create(self, request):
        data = request.data
        # print("heyseli")
        quiz = Quiz()
        user = Token.objects.filter(key=data['token'])
        if user.exists():
            user = user.last().user
        teacher = user
        # teacher = User.objects.get(username=data['teacher'])
        quiz.teacher = teacher
        quiz.title = data['title']
        quiz.clas_id = data['clas_id']
        quiz.save()

        order = 1
        for q in data['questions']:
            newQ = Question()
            newQ.question = q['title']
            newQ.order = order
            newQ.save()

            for c in q['choices']:
                temp = Choice.objects.filter(title=c)
                print(temp)
                if len(temp) == 0:
                    newC = Choice()
                    newC.title = c
                    newC.save()
                    newQ.choices.add(newC)
                else:
                    newQ.choices.add(temp[0])

            newQ.answer = Choice.objects.get(title=q['answer'])
            newQ.quiz = quiz
            newQ.save()
            order += 1
        return quiz


class GradedQuizSerializer(serializers.ModelSerializer):
    student = UserSerializer(many=False)
    quiz = QuizSerializer(many=False)

    class Meta:
        model = GradedQuiz
        fields = ('__all__')

    def create(self, request):
        data = request.data
        # print(data)

        quiz = Quiz.objects.get(id=data['asntId'])
        
        user = Token.objects.filter(key=data['token'])
        if user.exists():
            user = user.last().user
        student = user
        # student = User.objects.get(username=data['username'])
        het = GradedQuiz.objects.filter(student=student)
        het = het.filter(quiz=quiz)
        if(len(het) != 0):
            graded_asnt = het[0]
        else:
            graded_asnt = GradedQuiz()
            graded_asnt.quiz = quiz
            graded_asnt.student = student

        questions = [q for q in quiz.questions.all()]
        # print(data['answers'])
        # answers = [data['answers'][a] for a in data['answers']]

        answered_correct_count = 0
        for i in data['answers']:
            index = int(i)
            for j in questions:
                if j.order == index:
                    if j.answer.title == data['answers'][i]:
                        answered_correct_count += 1
                    break
        # for i in range(len(data['answers'])):
        #     print(questions[i].answer.title)
        #     if questions[i].answer.id == int(data['answers'][i][0]):
        #         if questions[i].answer.title == data['answers'][i][1]:
        #             answered_correct_count += 1
                # i += 1

        grade = answered_correct_count / len(questions) * 100
        graded_asnt.grade = grade
        graded_asnt.save()
        return graded_asnt


