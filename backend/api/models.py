from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    is_student = models.BooleanField(null=True,blank=True)
    is_teacher = models.BooleanField(null=True,blank=True)

    def __str__(self):
        return self.username

class Class(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(max_length=500,null=True,blank=True)
    teacher = models.ForeignKey(User, on_delete=models.CASCADE,related_name='teacher')
    students = models.ManyToManyField(User,related_name='students',blank=True)

    # def __init__(self, *args, **kwargs):
    #     super(Class, self).__init__(*args, **kwargs)
    #     self.fields['students'].required = False

    class Meta:
        unique_together = ('title', 'teacher',)

    def __str__(self):
        return self.title

class Choice(models.Model):
    title = models.CharField(max_length=50)

    def __str__(self):
        return self.title


class Quiz(models.Model):
    title = models.CharField(max_length=50)
    teacher = models.ForeignKey(User, on_delete=models.CASCADE)
    clas = models.ForeignKey(Class, on_delete=models.CASCADE,related_name='quiz')
    # questions = models.ManyToManyField(Question,related_name='questions')

    def __str__(self):
        return self.title

class Question(models.Model):
    question = models.CharField(max_length=200)
    choices = models.ManyToManyField(Choice)
    answer = models.ForeignKey(
        Choice, on_delete=models.CASCADE, related_name='answer', blank=True, null=True)
    quiz = models.ForeignKey(
        Quiz, on_delete=models.CASCADE, related_name='questions', blank=True, null=True)
    order = models.SmallIntegerField()

    def __str__(self):
        return self.question
        

class GradedQuiz(models.Model):
    student = models.ForeignKey(User, on_delete=models.CASCADE)
    quiz = models.ForeignKey(
        Quiz, on_delete=models.SET_NULL, blank=True, null=True)
    grade = models.FloatField()

    def __str__(self):
        return self.student.username






