from django.contrib import admin
from .models import User,Class, Quiz, Question, Choice, GradedQuiz


admin.site.register(User)
admin.site.register(Class)
admin.site.register(Quiz)
admin.site.register(Question)
admin.site.register(Choice)
admin.site.register(GradedQuiz)
