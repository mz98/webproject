from django.urls import path
from rest_framework import routers
from django.conf.urls import include
from .views import UserViewSet,QuizViewSet,GradedQuizListView,GradedQuizCreateView,ClassViewSet

router = routers.DefaultRouter()
router.register('users',UserViewSet)
router.register('gradedquiz',GradedQuizListView, 'gradeQuiz')
router.register('creategrade',GradedQuizCreateView, 'createQuiz')
router.register('quiz', QuizViewSet , 'quiz')
router.register('class', ClassViewSet , 'class')

urlpatterns = [
    path('', include(router.urls)),
]
