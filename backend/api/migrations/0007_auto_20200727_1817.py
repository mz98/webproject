# Generated by Django 3.0.8 on 2020-07-27 13:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0006_auto_20200727_1815'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='questions',
        ),
        migrations.AddField(
            model_name='question',
            name='quizes',
            field=models.ManyToManyField(related_name='questions', to='api.Quiz'),
        ),
    ]
