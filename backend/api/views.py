from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from django.shortcuts import render

from .models import User,Quiz, GradedQuiz,Question,Choice,Class
from .serializers import UserSerializer, QuizSerializer, GradedQuizSerializer,QuestionSerializer,ChoiceSerializer,ClassSerializer

from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST
)


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                       context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'is_student': user.is_student,
            'is_teacher': user.is_teacher
        })

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (AllowAny,)    

    def put(self, request, *args, **kwargs):
        print("helo")
        print(request)
        return self.updatereturnall(request, *args, **kwargs)

class QuizViewSet(viewsets.ModelViewSet):
    serializer_class = QuizSerializer
    # queryset = Quiz.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)    

    def get_queryset(self):
        queryset = Quiz.objects.all()
        token = self.request.query_params.get('token', None)
        user = Token.objects.filter(key=token)
        if user.exists():
            user = user.last().user
        # print(user)
        username = user
        if user is not None:
            if(user.is_teacher):
                queryset = queryset.filter(teacher__username=username)
                return queryset
            else:
                queryset = queryset.filter(clas__students__username=username)
                return queryset

    def create(self, request):
        # print("heyview ",request.data)
        serializer = QuizSerializer(data=request.data)
        serializer.is_valid()
        Quiz = serializer.create(request)
        if Quiz:
            return Response(status=HTTP_201_CREATED)
        return Response(status=HTTP_400_BAD_REQUEST)

class ClassViewSet(viewsets.ModelViewSet):
    serializer_class = ClassSerializer
    # queryset = Class.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)  

    def get_queryset(self):
        # quizset = Quiz.objects.all()
        queryset = Class.objects.all()
        token = self.request.query_params.get('token', None)
        user = Token.objects.filter(key=token)
        if user.exists():
            user = user.last().user
        # print(user)
        username = user
        # print(user.is_teacher)
        if user is not None:
            if(user.is_teacher):
                # quizset = quizset.filter(clas__title=)
                queryset = queryset.filter(teacher__username=username)
                # print(queryset)
                return queryset
            else:
                queryset = queryset.filter(students__username=username)
                # for cl in queryset:
                #     # print(cl)
                #     # print(type(cl))
                #     quizset = quizset.filter(clas__title=cl.title)
                #     cl.new_field = quizset
                #     cl.objects.annotate(number_of_entries=Quiz(quizset))
                #     # print(cl)
                #     # print(type(cl))
                #     print("cl",cl.new_field)
                #     print("quiz", quizset)
                # print("qury",queryset)
                return queryset
    def create(self, request):
        # print("heyview ",request.data)
        serializer = ClassSerializer(data=request.data)
        serializer.is_valid()
        clas = serializer.create(request)
        if clas:
            return Response(status=HTTP_201_CREATED)
        return Response(status=HTTP_400_BAD_REQUEST)


class GradedQuizListView(viewsets.ModelViewSet):
    serializer_class = GradedQuizSerializer

    def get_queryset(self):
        queryset = GradedQuiz.objects.all()
        # print(queryset)
        token = self.request.query_params.get('token', None)
        user = Token.objects.filter(key=token)
        if user.exists():
            user = user.last().user
        # print(user)
        username = user
        # username = self.request.query_params.get('username', None)
        if username is not None:
            is_teacher = self.request.query_params.get('teacher', None)
            # print(is_teacher)
            if(is_teacher == 'true'):
                is_teacher = True
            elif(is_teacher == 'false'):
                is_teacher = False
            if(is_teacher == True):
                # print(queryset)
                queryset = queryset.filter(quiz__teacher__username=username)
                # print(queryset)
            elif(is_teacher == False):
                # print(queryset)
                queryset = queryset.filter(student__username=username)
                # print(queryset)
            
        return queryset


class GradedQuizCreateView(viewsets.ModelViewSet):
    serializer_class = GradedQuizSerializer
    queryset = GradedQuiz.objects.all()

    def create(self, request):
        print(request.data)
        serializer = GradedQuizSerializer(data=request.data)
        serializer.is_valid()
        graded_Quiz = serializer.create(request)
        if graded_Quiz:
            return Response(status=HTTP_201_CREATED)
        return Response(status=HTTP_400_BAD_REQUEST)
