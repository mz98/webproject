import React from 'react';

import CanvasDraw from "react-canvas-draw";

class Whiteborad extends React.Component{
    state = {
        color: "#ffc600",
        width: 600,
        height: 400,
        brushRadius: 10,
        lazyRadius: 12
      };
      componentDidMount(){
        this.props.settitle("White Board");
        this.resize()
      }
      componentWillReceiveProps(newProps){
          if(newProps.collapsed !== this.props.collapsed){
            if (newProps.collapsed !== undefined && newProps.collapsed !== null) {
              let t = 0;
              if(newProps.collapsed){
                t= window.innerWidth - 400
              }else{
                t = window.innerWidth - 500
              }
              this.setState({width: t});
          }
        }
      }
      resize= (e)=>{
        let t= 2;
        if(this.props.collapsed){
          t= window.innerWidth - 400
        }else{
          t = window.innerWidth - 500
        }
        this.setState({width: t});
      }
    render(){
      window.onresize = this.resize
        return(
            <>
            <div style={{ width: this.state.width + 100,
                          display: 'grid',
                          gridTemplateColumns: 'auto auto auto auto',
                          gridGap: '10px 70px',
                          padding: '10px',
                        }}>
            {/* <div>
              <label>Width:</label><br/>
              <input
                type="number"
                value={this.state.width}
                onChange={e =>
                  this.setState({ width: parseInt(e.target.value, 10) })
                }
              />
            </div> */}
            <div>
              <label>Color:</label><br/>
              <input type="color" value={this.state.color}
                    onChange={e =>
                      this.setState({ color: e.target.value })
                    }
              />
            </div> 
            <div>
              <label>Height:</label><br/>
              <input
                type="number"
                value={this.state.height}
                onChange={e =>
                  this.setState({ height: parseInt(e.target.value, 10) })
                }
              />
            </div>
            <div>
              <label>Brush-Radius:</label><br/>
              <input
                type="number"
                value={this.state.brushRadius}
                onChange={e =>
                  this.setState({ brushRadius: parseInt(e.target.value, 10) })
                }
              />
            </div>
            <div>
              <label>Lazy-Radius:</label><br/>
              <input
                type="number"
                value={this.state.lazyRadius}
                onChange={e =>
                  this.setState({ lazyRadius: parseInt(e.target.value, 10) })
                }
              />
            </div>
          <div> 
              <button
              onClick={() => {
                this.saveableCanvas.clear();
              }}
            >
              Clear
            </button>
            </div>
            <div>
            <button
              onClick={() => {
                this.saveableCanvas.undo();
              }}
            >
              Undo
            </button></div>
            <div style={{gridRow: '2 / 5', gridColumn: '2/5'}}>
          <CanvasDraw
            ref={canvasDraw => (this.saveableCanvas = canvasDraw)}
            brushColor={this.state.color}
            brushRadius={this.state.brushRadius}
            lazyRadius={this.state.lazyRadius}
            canvasWidth={this.state.width}
            canvasHeight={this.state.height}
          /></div>
          </div>
          </>
        )
    }
}
export default Whiteborad;