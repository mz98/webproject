import React from "react";
import Result from './Result';
import { List, Avatar } from 'antd';
import { ProfileOutlined,CloseOutlined } from '@ant-design/icons';

class Grade extends React.Component {
  state = {
      data : [],
  }

  getgrade = ()=>{
    // console.log("heloo")
      fetch(`http://127.0.0.1:8000/api/gradedquiz/?token=${this.props.token}&teacher=${this.props.is_teacher}`,{
          method: 'GET',
          headers: {'Content-Type': 'application/json',
                  'Authorization': `Token ${this.props.token}`
          },
      })
      .then(resp => resp.json())
      .then(res => {
        console.log(res);
          this.setState({data: res})
      }).catch(err => {
          console.log(err)
      })
  }


  componentDidMount() {
    if (this.props.token !== undefined && this.props.token !== null) {
      this.props.settitle("Grades");
      // this.setState({is_student:this.props.is_student});
      // this.setState({is_teacher: this.props.is_teacher});
      this.getgrade();
    }
    else{
      window.location.href = '/';
    }
  }

  render() {{console.log(this.props.is_teacher)}
    return (  
      
        <>      
          <List
            itemLayout="vertical"
            pagination={{
              onChange: page => {
                console.log(page);
              },
              pageSize: 3,
            }}
            size="large"
            dataSource={this.state.data}
            renderItem={item => (
              // {console.log(item);}
              this.props.is_teacher != 'true'?
              <List.Item
                key={item.id}
                extra={
                  <Result key={item.id} grade={item.grade} />
                }
              >
                {item.quiz !== null?
                <List.Item.Meta
                  // avatar={<Avatar />}
                  avatar={
                    <Avatar style={{ backgroundColor: '#1890ff' }} icon={<ProfileOutlined />} />
                  }
                  title={item.quiz.title}
                  description={`Class: ${item.quiz.clas.title} && Teacher: ${item.quiz.teacher.username}`}
                /> 
                :
                <List.Item.Meta
                // avatar={<Avatar />}
                avatar={
                  <Avatar style={{ backgroundColor: '#1890ff' }} icon={<CloseOutlined />} />
                }
                title='Deleted quiz!'
                // description={`Teacher: ${item.quiz.teacher.username}`}
              /> 
          
                }
              </List.Item>
             :
             <List.Item
                key={item.id}
                extra={
                  <Result key={item.id} grade={item.grade} />
                }
              >
                {item.quiz !== null?
                <List.Item.Meta
                  // avatar={<Avatar />}
                  avatar={
                    <Avatar style={{ backgroundColor: '#1890ff' }} icon={<ProfileOutlined />} />
                  }
                  title={item.quiz.title}
                  description={`Class: ${item.quiz.clas.title} && Student: ${item.student.username}`}
                /> 
                :
                <List.Item.Meta
                // avatar={<Avatar />}
                avatar={
                  <Avatar style={{ backgroundColor: '#1890ff' }} icon={<CloseOutlined />} />
                }
                title='Deleted quiz!'
                // description={`Teacher: ${item.quiz.teacher.username}`}
              /> 
          
                }
              </List.Item>
            )
          }
          />
        </>
    );
  }
}

export default Grade;