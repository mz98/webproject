import React from "react";
import { Steps, Button } from "antd";

const Step = Steps.Step;

class Questions extends React.Component {
  state = {
    current: 0
  };

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  render() {
    const { current } = this.state;
    // const { questions } = this.props;
    // console.log(this.props.questions);
    return (
      <div>
        <Steps progressDot current={current}>
          {this.props.questions.map((q, index) => (
            <Step key={index} />
          ))}
        </Steps>
        <div>{this.props.questions[current]}</div>
        <div>
          {current > 0 && (
            <Button style={{ marginRight: 8 }} onClick={() => this.prev()}>
              Previous
            </Button>
          )}
          {current < this.props.questions.length - 1 && (
            <Button type="primary" onClick={() => this.next()}>
              Next
            </Button>
          )}
          {current === this.props.questions.length - 1 && (
            <Button type="primary" onClick={() => this.props.submit()}>
              Submit
            </Button>
          )}
          
        </div>
      </div>
    );
  }
}

export default Questions;
