import React from "react";
import { withCookies } from 'react-cookie';
import loginImg from "../../login.svg";

export class Login extends React.Component {
  state = {
    credent :{
      username : '',
      password : ''
    },
    haserror: false,
    errordescript :'',
    isPasswordShown : false,
  }

  inputChanged = event => {
    this.setState({haserror:false});
    this.setState({errordescript:''})
    let credent = this.state.credent;
    credent[event.target.name] = event.target.value;
    this.setState({credentials: credent});
  }
  login = event =>{
    if (this.state.credent.username != '' && this.state.credent.password != ''){
      fetch('http://127.0.0.1:8000/auth/',{
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(this.state.credentials)
      }).then(resp => resp.json())
      .then(res => {
        if (res['token']){
          this.setState({haserror:false});

          this.props.cookies(res.token,res.is_student,res.is_teacher);
          window.location.href = "/main";
        }
        else{
          // console.log(res['non_field_errors'][0]);
          this.setState({haserror:true});
          this.setState({errordescript:res['non_field_errors'][0]})
        }
        }).catch(err => {
          console.log(err)
        })
    }else{
      this.setState({haserror:true});
      this.setState({errordescript:'fill username and password'})
    }
  }

  togglePasswordVisiblity = () => {
    const { isPasswordShown } = this.state;
    this.setState({ isPasswordShown: !isPasswordShown });
  };

  render() {
    return (
      <div className="base-container" ref={this.props.containerRef}>
        <div className="header">Login</div>
        <div className="content">
          <div className="image">
            <img src={loginImg} />
          </div>
          <div className="form">
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <input type="text" name="username" placeholder="username"
                value={this.state.credent.username}
                onChange={this.inputChanged}
                className={`${this.state.haserror ? "error" : ""}`} />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input type={this.state.isPasswordShown ? "text" : "password"}
              name="password" placeholder="password" 
                value={this.state.credent.password} 
                onChange={this.inputChanged}
                className={`${this.state.haserror ? "error" : ""}`} />
              <i
                  className={`fa ${this.state.isPasswordShown ? "fa-eye-slash" : "fa-eye"} password-icon`}
                  onClick={this.togglePasswordVisiblity}
                />
            </div>
            <p className="errordescript">{this.state.errordescript}</p>
          </div>
        </div>
        <div className="footer">
          <button type="button" className="btn" 
            onClick={this.login}>
            Login
          </button>
        </div>
      </div>
    );
  }
}
export default withCookies(Login);