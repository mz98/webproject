import React from "react";
import { Radio } from "antd";

const RadioGroup = Radio.Group;

const radioStyle = {
  display: "block",
  height: "30px",
  lineHeight: "30px"
};

class Choices extends React.Component {
  render() {
    // const { questionId } = this.props;
    // const { usersAnswers } = this.props;
    return (
      <RadioGroup
        onChange={(e, qId) => this.props.change(e, this.props.questionId)}
        value={
          this.props.usersAnswers[this.props.questionId] !== undefined &&
          this.props.usersAnswers[this.props.questionId] !== null
            ? this.props.usersAnswers[this.props.questionId]
            : null
        }
      >
        {this.props.choices.map(c => {
          return (
            <Radio style={radioStyle} value={c["title"]} key={c["id"]}>
              {c["title"]}
            </Radio>
          );
        })}
      </RadioGroup>
    );
  }
}

export default Choices;
