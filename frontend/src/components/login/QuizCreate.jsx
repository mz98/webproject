import React from "react";
import { Form, Input, Button, Divider, Modal, message } from "antd";
import QuestionForm from "./QuestionForm";

import {
  MinusCircleOutlined,
  PlusOutlined,
  ExclamationCircleOutlined,
}from '@ant-design/icons';

const { confirm } = Modal;

const FormItem = Form.Item;

class QuizCreate extends React.Component {
  state = {
    formCount: 1,
    error : "",
    quest: [],
    listdel: [],
  };
  componentDidMount(){
    this.props.settitle("Quiz Create");
  }

  remove = (questions,i) => {
    console.log(questions)
    delete questions[i]
    console.log(questions)
    const { formCount } = this.state;
    this.setState({
      formCount: formCount - 1
    });
  };

  add = () => {
    const { formCount } = this.state;
    this.setState({
      formCount: formCount + 1
    });
  };

  handleSubmit = values => {
    // console.log("handle")
    const questions = [];
    this.setState({error: ''})
    let te = false;
    for (let i = 0; i < this.state.formCount; i += 1) {
      const choices = [];
      if(values[i] === undefined){
        this.setState({error: `You must add Choice to Question ${i}`})
      }else{
        for(let inn=0;inn< values[i].length;inn+=1){
          let tempch = values[i][inn]
          for(var propName in tempch) {
            if(tempch.hasOwnProperty(propName)) {
                var propValue = tempch[propName];
                choices.push(propValue)
            }
          }
          // choices.push(values[i][inn][inn][`questions[${i}]choices[${inn}]`])
        }
        // console.log(i," hh",choices, " vv", values[`answers[${i}]`])
        // console.log(choices[0] == values[`answers[${i}]`])
        for(let ch=0; ch < choices.length; ch +=1){
          if(choices[ch] ==  values[`answers[${i}]`]){
            te = true
          }
        }
        if(te == false){
          this.setState({error: `Answer of question ${i} is not in Choices.It must be in Choices`})
        }
        questions.push({
          title: values[`question[${i}]`],
          choices: choices,
          answer: values[`answers[${i}]`]
        });
      }  
    }
    if(this.state.error == ''){
      confirm({
        title: 'Do you Want to submit?',
        icon: <ExclamationCircleOutlined />,
        // content: 'Some descriptions',
        onOk: () => {
        message.success("Submitting your quiz!");
          const asnt = {
          token: this.props.token,
          title: values.title,
          clas_id: this.props.clas.id,
          questions
          };
          fetch(`http://127.0.0.1:8000/api/quiz/`,{
              method: 'POST',
              headers: {'Content-Type': 'application/json',
                      'Authorization': `Token ${this.props.token}`
              },        
              body: JSON.stringify(asnt),
          })
          // .then(resp => resp.json())
          .then(res => {
            if (res.status == 201){
              // console.log("ok");
              this.props.submitCreateQuiz();
            }else{
              // console.log("error");
              message.error("Something went Wrong. Try Again!");
            }
          }).catch(err => {
            console.log(err)
        })
        },
        onCancel: () => {
          console.log('Cancel');
          // this.setState({confirm: false})
        },
      });
    };
  }
  render() {
    const questions = []; 
    var num = 1;
    for (let i = 0; i < this.state.formCount; i += 1) {
      // console.log("cretequestion",i, questions)
      // console.log("dle",this.state.listdel.includes(i))
      if (this.state.listdel.includes(i)){continue}
      else{
      questions.push(
        <div key={i} >
          {questions.length > 0 ? (
                <MinusCircleOutlined 
                className="dynamic-delete-button"
                style={{color: 'red',
                        textAlign: 'left',
                        display: 'flex',
                        justifyContent: 'flex-end',
                        marginBottom: '10px'}}
                onClick={() => {
                  var  t= this.state.listdel.concat(i)
                  this.setState({listdel: t})
                }}/>
          ) : null}
          <QuestionForm id={i} formcount={this.state.formCount} num={num++}/>
          <Divider />
        </div>
      );}
    }
    // console.log(this.questions)
    return (
      <>
      <Form onFinish={this.handleSubmit}>
        {/* <h1>Create Quiz</h1> */}
        <FormItem label={"Title: "} name="title"
          validateTrigger={["onChange", "onBlur"]}
          rules={ [
            {
              required: true,
              message: "Please input a title"
            }
          ]}
        ><Input placeholder="Add a title" />
        </FormItem>
          {questions}
            <FormItem>
              <Button type="secondary" 
                    onClick={() => {this.add()}}>
                <PlusOutlined /> Add question
              </Button>
            </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </FormItem>
      </Form>
      <div
        style={{color:'red' , fontSize:'10'}}
      >{this.state.error}</div><br/>
      </>
    );
  }
}

export default QuizCreate;