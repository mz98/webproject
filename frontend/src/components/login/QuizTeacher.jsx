import React from "react";
import { Card, message, Modal } from "antd";
import {
  ExclamationCircleOutlined ,
} from '@ant-design/icons';

import ChoicesTeacher from "./ChoicesTeacher";
import QuestionsTeacher from "./QuestionsTeacher";
const { confirm } = Modal;

const cardStyle = {
  marginTop: "20px",
  marginBottom: "20px"
};
  
class QuizTeacher extends React.Component {
  state = {
    usersAnswers: {},
    is_student: null,
    is_teacher: null,
    quiz_id: null,
    confirm: false,
    // data: {},
  };

  // getquizDetail = (token)=>{
  //   // console.log("startquiz");
  //   fetch(`http://127.0.0.1:8000/api/quiz/${this.props.quiz_id}`,{
  //       method: 'GET',
  //       headers: {'Content-Type': 'application/json',
  //               'Authorization': `Token ${token}`
  //       },
  //   }).then(response => 
  //       response.json().then(data => ({
  //           data: data,
  //           status: response.status
  //       })
  //   ).then(res => {
  //       // console.log(res.status, res.data.title)
  //       // console.log(res.data)
  //       this.setState({data: res.data})
  //   }));
  //   // .then(resp => {resp.json()})
  //   // .then(res => {
  //   //   // console.log("getquiz")
  //   //     this.setState({data: res})
  //   // }).catch(err => {
  //   //     console.log(err)
  //   // })
  // }
  componentDidMount() {
    // if (this.props.token !== undefined && this.props.token !== null) {
    //   this.props.getASNTSDetail(this.props.token, this.props.match.params.id);
    // }
    if(this.props.token !== undefined && this.props.token !== null){
        // console.log("token" , this.props.token);
        this.props.settitle("Quiz Detail");
        this.setState({is_student:this.props.is_student});
        this.setState({is_teacher:this.props.is_teacher});
        // this.getquizDetail(this.props.token);
        // const iid = parseInt(this.props.match.params.id)
        // this.setState({quiz_id: iid}, function () { });
    }
    else{
        window.location.href = '/';
    }
  }
  componentDidUpdate(previousProps, previousState) {
    // console.log("update ",previousState.data,this.state.data)
    if (previousProps.data !== this.props.data) {
      this.setState({is_student:this.props.is_student});
      this.setState({is_teacher:this.props.is_teacher});
      // this.getquizDetail(this.props.token);
    }
  }
  componentWillReceiveProps(newProps) {
    // if (newProps.token !== this.props.token) {
    //   if (newProps.token !== undefined && newProps.token !== null) {
    //     this.props.getASNTSDetail(newProps.token, this.props.match.params.id);
    //   }
    // }
    if(newProps.token !== this.props.token){
      if (newProps.token !== undefined && newProps.token !== null) {
        // console.log("newpros");
        this.setState({is_student:this.props.is_student});
        this.setState({is_teacher:this.props.is_teacher});
        // this.getquizDetail(newProps.token);
      }else{
        window.location.href = '/';
      }
    }
  }

  onChange = (e, qId) => {
    const { usersAnswers } = this.state;
    usersAnswers[qId] = e.target.value;
    this.setState({ usersAnswers });
  };

  handleSubmit = ()=> {
    const token = this.props.token;
    const iid = this.props.data.id;
    const ans = this.state.usersAnswers;
    confirm({
      title: 'Do you Want to submit?',
      icon: <ExclamationCircleOutlined />,
      // content: 'Some descriptions',
      onOk: () => {
        message.success("Submitting your quiz!");
        const asnt = {
          token: token,
          asntId: iid,
          answers: ans
        };
        // this.props.createGradedASNT(this.props.token, asnt);
        fetch('http://127.0.0.1:8000/api/creategrade/',{
            method: 'POST',
            headers: {'Content-Type': 'application/json',
                    'Authorization': `Token ${token}`,
            },        
            body: JSON.stringify(asnt),
        })
        // .then(resp => resp.json())
        .then(res => {
            if (res.status == 201){
              this.setState({confirm:true})
              this.props.submitQuiz();
            }else{
              console.log("error");
              message.error("Something went Wrong. Try Again!");
            }
        }).catch(err => {
            console.log(err)
        })
      },
      onCancel: () => {
        // console.log('Cancel');
        this.setState({confirm: false})
        // return 1;
      },
    });
  }

  render() {
    return (
      <div>
        {console.log("hey",this.props.data )}
        {this.props.data !== undefined && this.props.data !== null ?
          Object.keys(this.props.data).length > 0 ? 
            <Card title={this.props.data.title}>
              <QuestionsTeacher
                  questions={this.props.data.questions.map(q => {
                  return (
                      <Card
                          style={cardStyle}
                          type="inner"
                          key={q.id}
                          title={`${q.order}. ${q.question}`}
                      >
                        <ChoicesTeacher
                            // questionId={q.order}
                            choices={q.choices}
                        />
                        <div>Answer: {q.answer.title}</div>
                      </Card>
                  );
                  })}
              />
          </Card>
          : null
        :null} 
      </div>
    );
  }
}


export default QuizTeacher;
