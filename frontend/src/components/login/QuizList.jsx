import React from "react";
import { List ,Avatar,Modal } from "antd";
import "./styleQuizlist.scss";
import {
    ProfileOutlined,
    ExclamationCircleOutlined,
} from '@ant-design/icons';

const { confirm } = Modal;

class QuizList extends React.Component{
    state = {
        is_student: null,
        is_teacher: null,
        data : [],
        selectedQuiz: null,
    }
    
    getquiz = ()=>{
        fetch(`http://127.0.0.1:8000/api/quiz/?token=${this.props.token}`,{
            method: 'GET',
            headers: {'Content-Type': 'application/json',
                    'Authorization': `Token ${this.props.token}`
            },
        }).then(resp => resp.json())
        .then(res => {
            this.setState({data: res})
        }).catch(err => {
            console.log(err)
        })
    }

    componentDidMount() {
        // if (this.props.token !== undefined && this.props.token !== null) {
        //   this.props.getASNTS(this.props.token);
        // }
        if(this.props.token !== undefined && this.props.token !== null){
            this.props.settitle("Quiz List");
            this.setState({is_student:this.props.is_student});
            this.setState({is_teacher: this.props.is_teacher});
            this.getquiz();
            
        }
        else{
          window.location.href = '/';
        }
    }
    
    componentWillReceiveProps(newProps) {
        // if (newProps.token !== this.props.token) {
        //     if (newProps.token !== undefined && newProps.token !== null) {
        //     this.props.getASNTS(newProps.token);
        //     }
        // }
    }    
    setDetail =(id,type) => (e)=>{
        // console.log(id)
        this.setState({selectedQuiz: id});
        this.props.quizClicked(id);
    }
    setDelete = (t) =>(e)=>{
        
    //   console.log("hey",t);
      var id = t.id;
      confirm({
        title: 'Sure to delete?',
        icon: <ExclamationCircleOutlined />,
        onOk: () => {
            fetch(`http://127.0.0.1:8000/api/quiz/${id}/?token=${this.props.token}`,{
                method: 'DELETE',
                headers: {'Content-Type': 'application/json',
                        'Authorization': `Token ${this.props.token}`
                },
            })
            // .then(resp => resp.json())
            .then(res => {
              if(res.status == 204){
                var temp = this.state.data
                var index = -1;
                for(let ii =0 ; ii < temp.length;ii+=1 ){
                  if( temp[ii] == t){
                    index = ii
                  }
                }
                temp.splice(index, 1);
                this.setState({data : temp})
              }
            }).catch(err => {
                console.log(err)
            })
        },
        onCancel: () => {
          console.log('Cancel');
        },
      });

    }
    
    // createQuiz = ()=>{
    //     this.props.createQuiz();
    // }
    
    renderItem(item) {
        return (
            // <Link to={`quiz/${item.id}`}>// </Link>
            <div>
            <List.Item key={item.id} 
                className="quizes"
            >
                <List.Item.Meta 
                className="itemquiz"
                avatar={
                    <Avatar style={{ backgroundColor: '#1890ff' }} icon={<ProfileOutlined />} />
                }
                title={item.title}
                description={`Class: ${item.clas.title} $$ Teacher Class: ${item.clas.teacher.username}`}
                />
                {/* <List.Item style={{zIndex: "1"}}> */}
                {this.props.is_teacher == 'true' ? 
                    <>
                    <div className="buttonDetail"
                        onClick={this.setDelete(item)}
                    >Delete </div>
                    |
                    </>
                :null}
                {this.props.is_teacher == 'true' ? 
                    <div className="buttonDetail"
                        onClick={this.setDetail(item.id,true)}
                    >Show Quiz</div>
                :
                    <div className="buttonDetail"
                        onClick={this.setDetail(item.id,false)}
                    >Start Quiz</div>
                }
                {/* </List.Item> */}
              </List.Item>
              <br className="brquiz"/>
            
            </div>
        );
    }

    render(){
        // console.log(this.state.data)
        return(
            <div >
                {/* <div id="wraper">
                <h3 style={{ margin: "16px 0" }}></h3>
                {this.state.is_teacher == 'true' ?                 
                <div id="createquizbotton" style={{ margin: "16px 0" }} 
                    onClick={this.createQuiz} >
                    <PlusOutlined />Create new quiz
                </div>
                    :null
                }
                </div> */}
                <div className="listquiz"><List
                size="large"
                bordered="true"
                pagination={{
                    onChange: page => {
                    //   console.log(page);
                    },
                    pageSize: 5,
                  }}
                dataSource={this.state.data}
                renderItem={item => this.renderItem(item)}
                /></div>
            </div>
        );
    }
}

export default QuizList;