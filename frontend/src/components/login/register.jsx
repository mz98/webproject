import React from "react";
import { withCookies } from 'react-cookie';
import loginImg from "../../login.svg";

export class Register extends React.Component {
  state = {
    credent :{
      username : '',
      password : '',
      is_student: null,
      is_teacher: null
    },
    selectValue : 'is_student',
    haserror: false,
    hasPasserror: false,
    errordescript :'',
    isPasswordShown : false,
  }

  inputChanged = event => {
    this.setState({haserror:false});
    this.setState({hasPasserror:false});
    this.setState({errordescript:''})
    let credent = this.state.credent;
    credent[event.target.name] = event.target.value;
    this.setState({credentials: credent});
  }

  register = event =>{
    if(this.state.selectValue == 'is_student'){
      let credent = this.state.credent;
      credent['is_student'] = true;
      credent['is_teacher'] = false;
      this.setState({credentials: credent});
    }else{
      let credent = this.state.credent;
      credent['is_student'] = false;
      credent['is_teacher'] = true;
      this.setState({credentials: credent});
    }
    if (this.state.credent.username != '' && this.state.credent.password != ''){
      fetch('http://127.0.0.1:8000/api/users/',{
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(this.state.credentials)
      }).then(resp => resp.json())
      .then(res => {
        if(typeof(res['username']) == 'object'){
          this.setState({haserror:true});
          this.setState({errordescript:res['username'][0]})
          
        }else{
          fetch('http://127.0.0.1:8000/auth/',{
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(this.state.credentials)
          }).then(resp => resp.json())
          .then(res => {
              this.setState({haserror:false});
              this.setState({hasPasserror:false});
              this.props.cookies(res.token,res.is_student,res.is_teacher);
              window.location.href = "/main";
            })
        }
      })
    }else{
      this.setState({haserror:true});
      this.setState({hasPasserror:true});
      this.setState({errordescript:'fill username and password'})
    }
  }

  handleChange = e =>{
    this.setState({selectValue:e.target.value})
  }

  togglePasswordVisiblity = () => {
    const { isPasswordShown } = this.state;
    this.setState({ isPasswordShown: !isPasswordShown });
  };

  render() {
    return (
      <div className="base-container" ref={this.props.containerRef}>
        <div className="header">Register</div>
        <div className="content">
          <div className="image">
            <img src={loginImg} />
          </div>
          <div className="form">
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <input type="text" name="username" placeholder="username"
                value={this.state.credent.username}
                onChange={this.inputChanged}
                className={`${this.state.haserror ? "error" : ""}`}
              />
            </div>
            
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input type={this.state.isPasswordShown ? "text" : "password"}
                name="password" placeholder="password" 
                value={this.state.credent.password} 
                onChange={this.inputChanged}
                className={`${this.state.hasPasserror ? "error" : ""}`}
              />
              <i
                className={`fa ${this.state.isPasswordShown ? "fa-eye-slash" : "fa-eye"} password-icon`}
                onClick={this.togglePasswordVisiblity}
              />
            </div>
            <div className="form-group">
              <label>You are </label>
              <select name="typeUser" 
              value={this.state.selectValue} 
              onChange={this.handleChange} >
                <option value="is_student" >Student</option>
                <option value="is_teacher" >Teacher</option>
              </select>
            </div>
            <p className="errordescript">{this.state.errordescript}</p>
          </div>
        </div>
        <div className="footer">
          <button type="button" className="btn" onClick={this.register}>
            Register
          </button>
        </div>
      </div>
    );
  }
}
export default withCookies(Register);