import React from "react";
import { Progress } from "antd";
import { Card } from 'antd';

const { Meta } = Card;

const Result = props => (
//   <Card
//     hoverable
//     style={{ width: 240 }}
//     cover={<Progress type="circle" percent={props.grade} width={80} />}
//   >
//     <Meta title={props.quiz.title} description={`Teacher: ${props.quiz.teacher.username}`} />
//   </Card>
  
  <Progress type="circle" percent={props.grade} width={80} />
);

export default Result;
