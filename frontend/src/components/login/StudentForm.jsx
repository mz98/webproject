import React from "react";
import { Form, Input, Button } from "antd";
import {
  MinusCircleOutlined,
  PlusOutlined,
}from '@ant-design/icons';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 2 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 },
  },
};
const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: { span: 24, offset: 0 },
    sm: { span: 20, offset: 2 },
  },
};

class StudentForm extends React.Component {
 
  render() {
    return (
      <>
        <Form.Item
          name={`name[${this.props.id}]`}
          label="User Name"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input />
        </Form.Item>
       {/* <FormItem
              label={`Question ${this.props.id}: `} 
              name={`question[${this.props.id}]`}
              validateTrigger={["onChange", "onBlur"]}
              rules={[
                {
                  required: true,
                  message: "Please input a question"
                }
              ]}
            ><Input placeholder="Add a question" />
            </FormItem>
            <FormItem 
              label="Answer: " 
              name={`answers[${this.props.id}]`}
              validateTrigger={["onChange", "onBlur"]}
              rules={[
                {
                  required: true,
                  message: "Please input an answer to this question"
                }
              ]}
            ><Input placeholder="What is the answer?" />
            </FormItem>
        <Form.List name={`${this.props.id}`}>
        
        {(fields, { add, remove }) => {
           return (
            <div>
            {fields.map((field, index) => (
                <Form.Item
                  {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
                  label={index === 0 ? "Choices" : ""}
                  key={field.key}
                >
                  <Form.Item
                    {...field}
                    label={`questions[${this.props.id}]choices[${field.key}]`}
                    name= {[index, `questions[${this.props.id}]choices[${field.key}]`]}
                    validateTrigger={['onChange', 'onBlur']}
                    rules={[
                      {
                        required: true,
                        whitespace: true,
                        message: "Please input a choice to the question",
                      },
                    ]}
                    noStyle
                  >
                    <Input placeholder="Answer choice" style={{ width: '60%' }}
                    />
                  </Form.Item>
                  {fields.length > 1 ? (
                    <MinusCircleOutlined
                      className="dynamic-delete-button"
                      style={{ margin: '0 8px' }}
                      onClick={() => {
                        remove(field.name);
                      }}
                    />
                  ) : null}
                    </Form.Item>
              ))
            }
            <FormItem>
              <Button type="dashed" onClick={() => add({})} style={{ width: "60%" }}>
                <PlusOutlined/> Add an answer choice
              </Button>
            </FormItem> 
          </div>
        )}}
        </Form.List> */}
      </>
    );
  }
}

export default StudentForm;