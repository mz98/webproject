import React from "react";
import "./style.scss";
import "./index.scss";
import { Login } from "./login";
import { Register } from "./register";
import { withCookies } from 'react-cookie';

// export { Login } from "./login";
// export { Register } from "./register";


export class FirstPage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        isLogginActive: true
      };
    }
  
    componentDidMount() {
      //Add .right by default
      this.rightSide.classList.add("right");
    }
  
    changeState() {
      const { isLogginActive } = this.state;
  
      if (isLogginActive) {
        this.rightSide.classList.remove("right");
        this.rightSide.classList.add("left");
      } else {
        this.rightSide.classList.remove("left");
        this.rightSide.classList.add("right");
      }
      this.setState(prevState => ({ isLogginActive: !prevState.isLogginActive }));
    }
  
    setcookie = (token , student , teacher) =>{
      let d = new Date();
      d.setTime(d.getTime() + (1*60*1000));
      console.log(d)

        this.props.cookies.set('user-token', token ,{expire: d});
        this.props.cookies.set('user-student', student);
        this.props.cookies.set('user-teacher', teacher);
    }

    render() {
      const { isLogginActive } = this.state;
      const current = isLogginActive ? "Register" : "Login";
      const currentActive = isLogginActive ? "login" : "register";
      return (
        <div className="FirstPage">
          <div className="login">
            <div className="container" ref={ref => (this.container = ref)}>
              {isLogginActive && (
                <Login containerRef={ref => (this.current = ref)} cookies={this.setcookie} />
              )}
              {!isLogginActive && (
                <Register containerRef={ref => (this.current = ref)} cookies={this.setcookie} />
              )}
            </div>
            <RightSide
              current={current}
              currentActive={currentActive}
              containerRef={ref => (this.rightSide = ref)}
              onClick={this.changeState.bind(this)}
            />
          </div>
        </div>
      );
    }
  }
  
  const RightSide = props => {
    return (
      <div
        className="right-side"
        ref={props.containerRef}
        onClick={props.onClick}
      >
        <div className="inner-container">
          <div className="text">{props.current}</div>
        </div>
      </div>
    );
  };
  
export default withCookies(FirstPage);
  