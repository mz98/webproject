import React from "react";
import { Form, Input, Button, Modal, message } from "antd";

import {
  MinusCircleOutlined,
  PlusOutlined,
  ExclamationCircleOutlined,
}from '@ant-design/icons';

const { confirm } = Modal;

const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 2 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 },
  },
};
const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: { span: 24, offset: 0 },
    sm: { span: 20, offset: 2 },
  },
};
class ClassCreate extends React.Component {
  state = {
    formCount: 0,
    error : "",
  };
  componentDidMount(){
    this.props.settitle("Class Create");
  }

  handleSubmit = values => {
    console.log("handle",values)
    this.setState({error: ''})
    confirm({
        title: 'Do you Want to submit?',
        icon: <ExclamationCircleOutlined />,
        onOk: () => {
            var des = '';
            var stu = [];
            if(values.description !== undefined){
                des = values.description
            }
            if(values.students !== undefined){
                stu = values.students
            }
          const asnt = {
            token: this.props.token,
            title: values.title,
            teacher: values.teacher,
            description : des,
            students : stu
          };
          fetch(`http://127.0.0.1:8000/api/class/`,{
              method: 'POST',
              headers: {'Content-Type': 'application/json',
                      'Authorization': `Token ${this.props.token}`
              },        
              body: JSON.stringify(asnt),
          })
          // .then(resp => resp.json())
          .then(res => {
            if (res.status == 201){
              message.success("Submitting your quiz!");
              console.log("ok");
              this.props.submitCreateClass();
            }else if (res.status == 400){
              console.log("error");
              this.setState({error: 'Your Teacher or Some of Your Students NOT Exists'})
              message.error("Something went Wrong. Try Again!");
            }else if(res.status == 500){
              console.log("error");
              this.setState({error: 'A Class with this title and Teacher already exists'})
              message.error("Something went Wrong. Try Again!");
            }
          })
        },
        onCancel: () => {
          console.log('Cancel');
        },
      });
  }
  render() {
    return (
      <>
      <Form onFinish={this.handleSubmit}>
        <FormItem label={"Title: "} name="title"
          validateTrigger={["onChange", "onBlur"]}
          rules={ [
            {
              required: true,
              message: "Please input a title"
            }
          ]}
        ><Input placeholder="Add a title" />
        </FormItem>
        <Form.Item name={'description'} label="Description">
                <Input.TextArea />
        </Form.Item>
        <FormItem label={"Teacher: "} name="teacher"
          validateTrigger={["onChange", "onBlur"]}
          rules={ [
            {
              required: true,
              message: "Please input a teacher username"
            }
          ]}
        ><Input placeholder="Add a teacher username" />
        </FormItem>
        
        <Form.List name="students">
        {(fields, { add, remove }) => {
          return (
            <div>
              {fields.map((field, index) => (
                <Form.Item
                {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
                  label={index === 0 ? 'Students: ' : ''}
                  required={false}
                  key={field.key}
                >
                  <Form.Item
                    {...field}
                    validateTrigger={['onChange', 'onBlur']}
                    rules={[
                      {
                        required: true,
                        whitespace: true,
                        message: "Please input student's username or delete this field.",
                      },
                    ]}
                    noStyle
                  >
                    <Input placeholder="Student username" style={{ width: '60%' }} />
                  </Form.Item>
                  {fields.length >= 1 ? (
                    <MinusCircleOutlined
                      className="dynamic-delete-button"
                      style={{ margin: '0 8px' ,color: 'red'}}
                      onClick={() => {
                        remove(field.name);
                      }}
                    />
                  ) : null}
                </Form.Item>
              ))}
              <Form.Item>
                <Button
                  type="dashed"
                  onClick={() => {
                    add();
                  }}
                  style={{ width: '60%' }}
                >
                  <PlusOutlined /> Add Student
                </Button>
              </Form.Item>
            </div>
          );
        }}
      </Form.List>
        <FormItem>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </FormItem>
      </Form>
      <div
        style={{color:'red' , fontSize:'10'}}
      >{this.state.error}</div><br/>
      </>
    );
  }
}

export default ClassCreate;