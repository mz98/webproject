import React from "react";
import { Card, List, Modal } from "antd";
import {
    BarsOutlined,
    PlusOutlined,
    DeleteOutlined,
    ExclamationCircleOutlined,
} from '@ant-design/icons';

import blue from './blue.jpg';
import "./styleClassList.scss";
import 'antd/dist/antd.css';
const { Meta } = Card;
const { confirm } = Modal;
const colCounts = {};


[1,2, 3, 4, 6, 8, 12].forEach((value, i) => {
  colCounts[i] = value;
});

class ClassList extends React.Component{
    state = {
        colCountKey: null,
        data : '',
      };    
    getclass = ()=>{
      fetch(`http://127.0.0.1:8000/api/class/?token=${this.props.token}`,{
          method: 'GET',
          headers: {'Content-Type': 'application/json',
                  'Authorization': `Token ${this.props.token}`
          },
      }).then(resp => resp.json())
      .then(res => {
          this.setState({data: res})
      }).catch(err => {
          console.log(err)
      })
  }
    componentDidMount(){
      if(this.props.token !== undefined && this.props.token !== null){
        this.props.settitle("Class List");
        this.setState({is_student:this.props.is_student});
        this.setState({is_teacher: this.props.is_teacher});
        this.getclass();
        
        
      let t= 2;
      if(this.props.collapsed){

        if(window.innerWidth <= 600 - 200){
          t = 0;
        }else if(window.innerWidth < 1000 -200){
          t = 1;
        }
      }else{
      
        if(window.innerWidth <= 600 - 80){
          t = 0;
        }else if(window.innerWidth < 1000 -80){
          t = 1;
        }
      }
      this.setState({colCountKey: t});
      }
      else{
        window.location.href = '/';
      }
    }
    componentWillUnmount() {
      this.setState = (state,callback)=>{
          return;
      };
    }
    resize= (e)=>{
      let t= 2;
      if(this.props.collapsed){

        if(window.innerWidth <= 600 - 200){
          t = 0;
        }else if(window.innerWidth < 1000 -200){
          t = 1;
        }
      }else{
      
        if(window.innerWidth <= 600 - 80){
          t = 0;
        }else if(window.innerWidth < 1000 -80){
          t = 1;
        }
      }
      this.setState({colCountKey: t});
    }

    delete = (t)=>{

      console.log("hey",t);
      var id = t.id;
      confirm({
        title: 'Sure to delete?',
        icon: <ExclamationCircleOutlined />,
        onOk: () => {
            fetch(`http://127.0.0.1:8000/api/class/${id}/?token=${this.props.token}`,{
                method: 'DELETE',
                headers: {'Content-Type': 'application/json',
                        'Authorization': `Token ${this.props.token}`
                },
            })
            // .then(resp => resp.json())
            .then(res => {
              console.log(res)
              if(res.status == 204){
                var temp = this.state.data
                console.log(temp)
                var index = -1;
                for(let ii =0 ; ii < temp.length;ii+=1 ){
                  if( temp[ii] == t){
                    console.log(ii,"true")
                    // delete temp[ii]
                    index = ii
                  }
                }
                temp.splice(index, 1);
                console.log(temp)
                this.setState({data : temp})
              }
              // console.log(this.state.data)
              // delete(this.state.data.t)
            }).catch(err => {
                console.log(err)
            })
        },
        onCancel: () => {
          console.log('Cancel');
        },
      });
    }
    more = (t)=>{
      console.log("wde",t);
      this.props.classClicked(t);
    }
    createClass = ()=>{
      this.props.createClass();
    }

    render(){
      window.onresize = this.resize
      return(
        <>
        <div id="wraper">
          <h3 style={{ margin: "16px 0" }}></h3>
          {this.state.is_teacher == 'true' ?                 
          <div id="createquizbotton" style={{ margin: "16px 0" }} 
              onClick={this.createClass} >
              <PlusOutlined />Create new class
          </div>
              :null
          }
        </div>
  
        <List
          grid={{ gutter: 55, column: colCounts[this.state.colCountKey] }}
          pagination={{
            onChange: page => {
              console.log(page);
            },
            pageSize: 6,
          }}
          dataSource={this.state.data}
          renderItem={item => (
            <List.Item>
              <Card 
                  style={{ width: '25vw',cursor:'default' }}
                  cover={
                  <img
                      alt="class"
                      src = {blue}
                      // src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                  />
                  }
                  actions={
                      this.props.is_teacher==='true' ? [
                      <BarsOutlined key="more" onClick={() => this.more(item)}/>,
                      <DeleteOutlined key="delete" onClick={() => this.delete(item)}/>
                      ]
                      :
                      [<BarsOutlined key="more" onClick={() => this.more(item)}/>
                      ]
                  }
                  hoverable={true}
              >
                  <Meta style={{ width: 'inherit',justifyContent: 'center' }}
                  // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                  title={item.title}
                  // description={item.description}
                  />
              </Card>
            </List.Item>
          )}
        />
        </>
      );
    }
}


export default ClassList;