import React from 'react';
import { Table, Input, Button, Space } from 'antd';
import Highlighter from 'react-highlight-words';
import { SearchOutlined,PlusOutlined } from '@ant-design/icons';
import { Drawer, Form, Col, Row, Select, message, Popconfirm } from 'antd';

const { Option } = Select;

class Admin extends React.Component {
  state = {
    searchText: '',
    searchedColumn: '',
    sortedInfo: null,
    filteredInfo: null,
    data: [],
    visible: false,
    errordescript :'',
  };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  getdata = ()=>{
    fetch(`http://127.0.0.1:8000/api/users/`,{
        method: 'GET',
        headers: {'Content-Type': 'application/json',
                'Authorization': `Token ${this.props.token}`
        },
    }).then(resp => resp.json())
    .then(res => {
        // console.log(res)
        var temp = []
        for( let i =0 ; i<res.length;i +=1){
            var ele = {}
            ele.key = res[i].id
            ele.username = res[i].username
            ele.email = res[i].email
            if(res[i].is_student == true){
                ele.type = 'student';
            }
            else if(res[i].is_teacher == true){
                ele.type = 'teacher';
            }
            temp.push(ele)
        }
        this.setState({data: temp})
    }).catch(err => {
        console.log(err)
    })
  }

  componentDidMount(){
    if(this.props.token !== undefined && this.props.token !== null){
      this.props.settitle("User List");
      this.getdata();
    }
    else{
      window.location.href = '/';
    }
  }

  handleChange = (pagination,filters, sorter) => {
    // console.log('Various parameters', pagination,filters, sorter);
    this.setState({
      sortedInfo: sorter,
      filteredInfo: filters,
    });
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex] ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()) : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  handleSubmit = (values)=>{
    //   console.log(values)
      var credent ={
          username: values.username,
          password: values.password,
          email :'',
          is_student: null,
          is_teacher: null,
      }
      if(values.type == 'is_student'){
        credent['is_student'] = true;
        credent['is_teacher'] = false;
      }else{
        credent['is_student'] = false;
        credent['is_teacher'] = true;
      }
      if(values.email === undefined){
          credent['email'] = ''
      }else{
          credent['email'] = values.email
      }
      if (credent.username != '' && credent.password != ''){
        fetch('http://127.0.0.1:8000/api/users/',{
          method: 'POST',
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify(credent)
        }).then(resp => resp.json())
        .then(res => {
          if(typeof(res['username']) == 'object'){
            this.setState({errordescript:res['username'][0]})
          }else{
            this.setState({errordescript: ''})
            // console.log(res)
            message.success("New user Created!");
            var ele = {}
            ele.key = res.id
            ele.username = res.username
            ele.email = res.email
            if(res.is_student == true){
                ele.type = 'student';
            }
            else if(res.is_teacher == true){
                ele.type = 'teacher';
            }
            var temp = this.state.data
            temp.push(ele)
            this.setState({data: temp})
            this.onClose();
            // this.props.submitUser();
          }
        })
      }else{
        this.setState({errordescript:'fill username and password'})
      }
  }
  handleDelete = key => {
    // console.log(key)
    fetch(`http://127.0.0.1:8000/api/users/${key}/`,{
        method: 'DELETE',
        headers: {'Content-Type': 'application/json',
                'Authorization': `Token ${this.props.token}`
        },
    })
    // .then(resp => resp.json())
    .then(res => {
      if(res.status == 204){
        const dataSource = [...this.state.data];
        this.setState({ data: dataSource.filter(item => item.key !== key) });
      }
    }).catch(err => {
        console.log(err)
    })
  };

  render() {
    let { sortedInfo, filteredInfo } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const columns = [
      {
        title: 'UserName',
        dataIndex: 'username',
        key: 'username',
        width: '40%',
        sorter: (a, b) => a.username.length - b.username.length,
        sortOrder: sortedInfo.columnKey === 'username' && sortedInfo.order,
        ...this.getColumnSearchProps('username'),
      },
      {
        title: 'Type',
        dataIndex: 'type',
        key: 'type',
        width: '20%',
        filters: [
            {
              text: 'student',
              value: 'student',
            },
            {
              text: 'teacher',
              value: 'teacher',
            },
          ],
        //   filterMultiple: false,
          filteredValue: filteredInfo.type || null,
          onFilter: (value, record) => record.type.includes(value),
        // ...this.getColumnSearchProps('type'),
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        ...this.getColumnSearchProps('email'),
      },
      {
        title: 'Action',
        dataIndex: 'action',
        render: (text, record) =>
          this.state.data.length >= 1 ? (
            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.key)}>
              <a>Delete</a>
            </Popconfirm>
          ) : null,
      },
    ];
    return (
    <>
        <Drawer
          title="Create a new account"
          width={500}
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 80 }}
        >
          <Form layout="vertical" onFinish={this.handleSubmit}>
          <p style={{color: 'red'}}>{this.state.errordescript}</p>
            <Row >
              <Col span={20}>
                <Form.Item
                  name="username"
                  label="User Name"
                  rules={[{ required: true, message: 'Please enter user name' }]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
            </Row>
            <Row >
              <Col span={20}>
                <Form.Item
                  name="password"
                  label="Password"
                  rules={[
                    {
                      required: true,
                      message: 'Please input your password!',
                    },
                  ]}
                  hasFeedback
                >
                  <Input.Password  placeholder="Please enter password" />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={20}>
                <Form.Item
                  name="email"
                  label="Email"
                  rules={[
                    {
                      type: 'email',
                      message: 'The input is not valid Email!',
                    },
                  ]}
                >
                  <Input placeholder="Please enter Email"  />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={20}>
                <Form.Item
                  name="type"
                  label="Type"
                  rules={[{ required: true, message: 'Please choose the type' }]}
                >
                  <Select placeholder="Please choose the type">
                    <Option value="is_student">Student</Option>
                    <Option value="is_teacher">Teacher</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Form.Item>
            <div
              style={{
                textAlign: 'right',
              }}
            >
              <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                Cancel
              </Button>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </div>
            </Form.Item>
          </Form>
        </Drawer>
    <Table columns={columns} dataSource={this.state.data} 
            onChange={this.handleChange}
            pagination={{
                onChange: page => {
                    // console.log(page);
                },
                pageSize: 5,
                }}
    />
    <Button type="primary" onClick={this.showDrawer}>
          <PlusOutlined /> New account
    </Button>
    </>
    )
  }
}

export default Admin;