import React from "react";

class ChoicesTeacher extends React.Component {
  render() {
    return (
      <ul type='circle' >
        {this.props.choices.map(c => {
          return (
            <li key={c["id"]}>
              {c["title"]}
            </li>
          );
        })}
      </ul>
    );
  }
}

export default ChoicesTeacher;
