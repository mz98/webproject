import React from "react";
import { Descriptions, Table, Button } from 'antd';

import {
  PlusOutlined ,
} from '@ant-design/icons';

class ClassDetail extends React.Component {
  state = {
    is_student: null,
    is_teacher: null,
    data: {},
  };

  componentDidMount() {
    if(this.props.token !== undefined && this.props.token !== null){
        this.props.settitle("Class Detail");
        this.setState({is_student:this.props.is_student});
        this.setState({is_teacher:this.props.is_teacher});
        this.setState({data: this.props.clas})
        }
    else{
        window.location.href = '/';
    }
  }
  whiteborad = ()=>{
    this.props.whiteborad();
  }
  
//   <QuizCreate token={this.state.token}
//   settitle={this.settitle}
//   submitCreateQuiz={this.submitCreateQuiz}
// />
  createQuiz=()=>{
    this.props.createQuiz();
  }
  render() {
    return (
      <>
      <div id="wraper">
          <h3 style={{ margin: "16px 0" }}></h3>
          {this.state.is_teacher == 'true' ?                 
          <div id="createquizbotton" style={{ margin: "16px 0" }} 
              onClick={this.createQuiz} >
              <PlusOutlined />Create new Quiz
          </div>
              :null
          }
        </div>
      {/* {console.log(this.state.data.teacher)} */}
        <Descriptions title={`${this.state.data.title}`} layout="vertical" bordered>
        <Descriptions.Item label="Teacher" span={2}>{`${this.props.clas.teacher.username}`}</Descriptions.Item>
        <Descriptions.Item label="Email Teacher">{`${this.props.clas.teacher.email}`}</Descriptions.Item>
        <Descriptions.Item label="Description" span={3} 
        >
          <div style={{display: 'flex', flexDirection: 'row' , justifyContent: 'space-between'}}>
            {this.state.data.description}
            <Button onClick={this.whiteborad}>WhiteBoard</Button>
            </div>
        </Descriptions.Item>
        <Descriptions.Item label="Students" span={3}>
            <Table
              columns={[
                { title: 'UserName', dataIndex: 'username' },
                { title: 'Email', dataIndex: 'email' },
              ]}
              pagination={{
                onChange: page => {},
                pageSize: 5,
              }}
              dataSource={
                  this.props.clas.students
              }
              rowKey='id'
            />
        </Descriptions.Item>
    </Descriptions>,
      </>
    );
  }
}

export default ClassDetail;
