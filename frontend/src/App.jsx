import React from "react";
import "./App.scss";
import { withCookies,Cookies } from 'react-cookie';
import QuizList from './components/login/QuizList'
import QuizCreate from './components/login/QuizCreate';
import QuizDetail from "./components/login/QuizDetail";
import ClassList from "./components/login/ClassList";
import ClassDetail from './components/login/ClassDetail';
import ClassCreate from "./components/login/ClassCreateF";
import Grade from './components/login/Grade';
import Admin from './components/login/Admin';
import Whiteborad from './components/login/Whiteborad';
import QuizTeacher from "./components/login/QuizTeacher";
import { Layout, Menu,
  PageHeader,
   Button,
  } from 'antd';
import {
  LogoutOutlined,
  ArrowLeftOutlined,
  UserOutlined,
  BookOutlined,
  FormOutlined,
  SolutionOutlined,
} from '@ant-design/icons';

const { Sider, Content } = Layout;

// function showConfirm() {
//   confirm({
//     title: 'Do you Want to delete these items?',
//     icon: <ExclamationCircleOutlined />,
//     content: 'Some descriptions',
//     onOk() {
//       console.log('OK');
//     },
//     onCancel() {
//       console.log('Cancel');
//     },
//   });
// }

class App extends React.Component {

  state = {
    token: this.props.cookies.get('user-token'),
    student: this.props.cookies.get('user-student'),
    teacher: this.props.cookies.get('user-teacher'),
    collapsed: false,
    selectMenu: "1",
    disableMenu : false,
    defaultSelected : ['1'],
    selectedQuiz: null,
    selectedClass: null,
    detailSelectedQuiz: null,
    createQuiz: false,
    createClass: false,
    setcreateQuiz: false,
    whiteborad:false,
    titlePage: "Hello"
  }

  componentDidMount(){
    if(this.state.token){
      // console.log(this.state.teacher==='true')
      // console.log(typeof(this.state.teacher))
    }
    else{
      window.location.href = '/';
    }
  }
  componentDidUpdate(){
    if(this.state.token){
    }
    else{
      window.location.href = '/';
    }
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  removetoken = ()=>{
    this.setState({token:null})
    this.setState({student:null})
    this.setState({teacher: null})
    const cookie= new Cookies();
    cookie.remove("user-token")
    cookie.remove("user-student")
    cookie.remove("user-teacher")
  }

  selectMenu= (e)=>{
    this.setState({selectMenu: e.key})
  }

  classClicked = (clas)=>{
    // console.log(clas)
    this.setState({disableMenu: true})
    this.setState({selectedClass: clas})
  }
  whiteborad = ()=>{
    this.setState({disableMenu:true})
    this.setState({whiteborad:true})
  }
  quizClicked= (quiz_id)=>{
    this.setState({disableMenu: true})
    this.setState({selectedQuiz: quiz_id})
    fetch(`http://127.0.0.1:8000/api/quiz/${quiz_id}/?token=${this.state.token}`,{
        method: 'GET',
        headers: {'Content-Type': 'application/json',
                'Authorization': `Token ${this.state.token}`
        },
    }).then(response => 
        response.json().then(data => ({
            data: data,
            status: response.status
        })
    ).then(res => {
        this.setState({detailSelectedQuiz: res.data})
    })).catch(err => {
      console.log(err)
  });
  }

  submitQuiz=()=>{
    this.setState({selectedQuiz:null});
    this.setState({disableMenu:false})
  }

  createQuiz=()=>{
    this.setState({disableMenu:true});
    this.setState({createQuiz:true});
  }
  createClass=()=>{
    this.setState({disableMenu:true});
    this.setState({createClass:true});
  }
  setcreateQuiz=()=>{
    this.setState({disableMenu: true})
    this.setState({setcreateQuiz: true})
  }
  submitCreateQuiz=()=>{
    this.setState({disableMenu:false});
    this.setState({createQuiz:false});
    this.setState({setcreateQuiz: false})
  }
  
  submitCreateClass=()=>{
    this.setState({disableMenu:false});
    this.setState({createClass:false});
  }
  onCollapse = collapsed => {
    this.setState({ collapsed });
  };
  settitle = (title)=>{
    this.setState({titlePage : title})
  }
  submitUser=()=>{
    console.log("hel")
    this.reload();
  }
  reload = ()=>{
    const select = this.state.selectMenu;
    this.setState({selectedQuiz : null})
    this.setState({createQuiz : false})
    this.setState({createClass: false})
    this.setState({disableMenu:false})
    this.setState({defaultSelected : select})
    this.setState({selectedClass : null})
    this.setState({setcreateQuiz: false})
    this.setState({whiteborad:false})
    // window.location.reload(false)
    this.forceUpdate() 
  }
  
  render() {
    return (
      <Layout>
      <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
        <div className="logo">
          <br/><br/></div>
        <Menu 
          className="fixedmenu" theme="dark" mode="inline" 
          defaultSelectedKeys={this.state.defaultSelected} 
          onSelect={this.selectMenu}>        
          <Menu.Item disabled={this.state.disableMenu} key="1" icon={<BookOutlined  />}>
            Classes
          </Menu.Item>
          <Menu.Item disabled={this.state.disableMenu} key="2" icon={<FormOutlined />}>
            Quizes
          </Menu.Item>
          <Menu.Item disabled={this.state.disableMenu} key="3" icon={<SolutionOutlined />}>
            Grades
          </Menu.Item>
          <Menu.Item disabled={this.state.disableMenu} key="4" icon={<UserOutlined />}>
            Admin
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        {/* <Header className="site-layout-background fixedheader" style={{ padding: 0 }}> */}
        <PageHeader
            className="fixed header"
            ghost={false}
            onBack={this.reload}
            backIcon={
              (this.state.selectedQuiz !== null ||
              this.state.createQuiz !== false ||
              this.state.selectedClass !== null ||
              this.state.createClass !== false ||
              this.state.setcreateQuiz !== false ||
              this.state.whiteborad !== false
              ) ?
              <ArrowLeftOutlined />
              : false
            }
            title={this.state.titlePage}
            // subTitle="This is a subtitle"
            extra={[
              <Button key="1" onClick={this.removetoken}>
              <LogoutOutlined />
               log out
              </Button>
            ]}
          >
          </PageHeader>
        <Content
          className="site-layout-background"
          style={{
            margin: '89px 16px',
            padding: 24,
            minHeight: 465,
          }}
        >
          {this.state.selectMenu ==2 ? 
            this.state.selectedQuiz === null ?
              this.state.createQuiz === null || 
              this.state.createQuiz === false ?
                <QuizList token={this.state.token} 
                          is_student={this.state.student}
                          is_teacher={this.state.teacher}
                          quizClicked={this.quizClicked}
                          createQuiz={this.createQuiz}
                          settitle={this.settitle}
                />     
              :null
              // <QuizCreate token={this.state.token}
              //             settitle={this.settitle}
              //             submitCreateQuiz={this.submitCreateQuiz}
              // />
            :
            this.state.teacher == 'false'?
              <QuizDetail token={this.state.token} 
                          is_student={this.state.student}
                          is_teacher={this.state.teacher}
                          quiz_id={this.state.selectedQuiz}
                          data={this.state.detailSelectedQuiz}
                          submitQuiz={this.submitQuiz}
                          settitle={this.settitle}
                />  
            :
              <QuizTeacher token={this.state.token} 
                          is_student={this.state.student}
                          is_teacher={this.state.teacher}
                          quiz_id={this.state.selectedQuiz}
                          data={this.state.detailSelectedQuiz}
                          // submitQuiz={this.submitQuiz}
                          settitle={this.settitle}
              />    
          :
          this.state.selectMenu ==1 ?
          this.state.selectedClass === null ?
            this.state.createClass === null || 
            this.state.createClass === false ?
            <ClassList  token={this.state.token}
                        collapsed={this.state.collapsed}
                        is_student={this.state.student}
                        is_teacher={this.state.teacher}
                        settitle={this.settitle}
                        classClicked={this.classClicked}
                        createClass={this.createClass}
            />
            :
            <ClassCreate
                        token={this.state.token}
                        // collapsed={this.state.collapsed}
                        is_student={this.state.student}
                        is_teacher={this.state.teacher}
                        submitCreateClass={this.submitCreateClass}
                        settitle={this.settitle}
            />
          :this.state.setcreateQuiz !== true ? 
          this.state.whiteborad === false ?
            <ClassDetail token={this.state.token} 
                            is_student={this.state.student}
                            is_teacher={this.state.teacher}
                            clas={this.state.selectedClass}
                            createQuiz={this.setcreateQuiz}
                            whiteborad={this.whiteborad}
                            settitle={this.settitle}
            />
            :    
            <Whiteborad collapsed={this.state.collapsed}
                        settitle={this.settitle}
            />

            :
            <QuizCreate token={this.state.token}
                        clas={this.state.selectedClass}
                        settitle={this.settitle}
                        submitCreateQuiz={this.submitCreateQuiz}
            />
            
          :
          this.state.selectMenu == 3 ?
            <Grade token={this.state.token} 
            is_student={this.state.student}
            is_teacher={this.state.teacher}
                   settitle={this.settitle}
            />
          :this.state.selectMenu == 4?
          <Admin token={this.state.token} 
                 settitle={this.settitle}
                 submitUser={this.submitUser}
          />
          :null
        }
          
        </Content>
      </Layout>
    </Layout>

    );
  }
}

export default withCookies(App);
